MenuObj = {}

function MenuObj.addItem(name, text)
	local MenuItem = {}
	MenuItem.text = text
	MenuItem.name = name
	MenuItem.selected = false


	index = MenuObj.getSize()
	MenuObj[index] = {}
	MenuObj[index].item = MenuItem
end

function MenuObj.getSize()
	count = 0
	for index, item in pairs(MenuObj) do
		count = count + 1
	end

	return count
end

function MenuObj.getSelectedIndex()
	for i = 1, i < table.getn(MenuObj), 1 do
		if MenuObj[i].selected then
			return i
		end
	end
end
