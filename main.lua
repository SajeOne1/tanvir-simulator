require 'states'

background = {}
title = {}
city = {}
player = {}
ai = {}
block = {}
BLOCK_COUNT = 3
state = GameStates.playing

gameTime = 0


function love.load()
	background.img = love.graphics.newImage('textures/background.png')
	background.wRaw = 1920
	background.hRaw = 1080
	background.scale = (love.graphics.getWidth() / background.hRaw)

	title.img = love.graphics.newImage('textures/title.png')
	title.x = (love.graphics.getWidth() / 2) - (title.img:getWidth() / 2)
	title.y = love.graphics.getHeight() / 4

	city.img = love.graphics.newImage('textures/city.png')
	city.speed = 500
	city.x1 = 0
	city.x2 = city.img:getWidth()
	city.y = love.graphics.getHeight() - (city.img:getHeight() - 150)

	player.x = love.graphics.getWidth() / 2
	player.y = love.graphics.getHeight() / 2
	player.width = 85
	player.height = 85
	player.img = love.graphics.newImage('textures/player.png')
	player.speed = 400
	player.health = 100
	player.damaged = false
	player.orientation = 0

	ai.x = -400
	ai.y = 200
	ai.width = 85
	ai.height = 85
	ai.img = love.graphics.newImage('textures/ai.png')
	ai.speed = 1
	ai.snaggedV = false
	ai.snaggedI = 0

	
	for i = 1, BLOCK_COUNT, 1 do
		block[i] = initializeBlock(i)
	end
end

function love.draw()
	love.graphics.draw(background.img, 0, 0, 0, background.scale, background.scale, 0, 0)
	love.graphics.draw(city.img, city.x1, city.y, 0, 1, 1, 0, 0)
	love.graphics.draw(city.img, city.x2, city.y, 0, 1, 1, 0, 0)

	love.graphics.print("Tanvir Simulator v0.1", 10, 10)
	love.graphics.print("Game Time: " .. gameTime, 10, love.graphics.getHeight() - 20)

	if state == GameStates.mainmenu then
		drawMainMenu()
	elseif state == GameStates.playing then
		drawPlaying()
	end

	
end


function love.update(dt)
	gameTime = gameTime + dt
	ai.speed = (gameTime * 10) + 400
	
	if state == GameStates.playing then
		playerUpdate(dt)
		aiUpdate(dt)	
	end

	moveBlock(dt)
	moveCity(dt)
end

function moveBlock(dt)
	for i=1, BLOCK_COUNT, 1 do
		if block[i].x < -block[i].width then
			block[i].x = love.graphics.getWidth() + block[i].width
			block[i].y = love.math.random(0, love.graphics.getHeight() - block[i].height)
		else
			block[i].x = block[i].x - (block[i].speed*dt)
		end

		if (ai.snaggedV == false) and collision(block[i].x, ai.x, block[i].y, ai.y, block[i].width, ai.width, block[i].height, ai.height) then
			ai.snaggedV = true
			ai.snaggedI = i
		end

		if collision(player.x, block[i].x, player.y, block[i].y, player.width, block[i].width, player.height, block[i].height) then
			if player.health > 0 then
				player.health = player.health - 1
			end
			player.damaged = gameTime
		end
	end

	if(ai.snaggedV ~= false) then
		ai.snaggedV = block[ai.snaggedI].x - ai.width
	end
end

function aiUpdate(dt)
	local distX = ai.x - player.x
	local distY = ai.y - player.y

	local hyp = math.sqrt((distX * distX) + (distY * distY)) / (ai.speed*dt / 10)
	distX = distX / hyp
	distY = distY / hyp

	if not ai.snaggedV then
		ai.x = ai.x - distX
		ai.y = ai.y - distY
	else
		ai.x = ai.snaggedV
		if ai.x <= 0 then
			ai.snaggedV = false
		end
	end
end

function moveCity(dt)
	if gameTime <= 6 then
		city.speed = 2400 / gameTime
	end

	if (city.x1 + city.img:getWidth()) <= 0 then
		city.x1 = city.x2 + city.img:getWidth()
	else
		city.x1 = city.x1 - (city.speed * dt)
	end

	if (city.x2 + city.img:getWidth()) <= 0 then
		city.x2 = city.x1 + city.img:getWidth()
	else
		city.x2 = city.x2 - (city.speed * dt)
	end
end

function playerUpdate(dt)
	if love.keyboard.isDown('d') then
		player.x = player.x + (player.speed*dt)
	elseif love.keyboard.isDown('a') then
		player.x = player.x - (player.speed*dt)
	end
	if love.keyboard.isDown('w') then
		player.y = player.y - (player.speed*dt)
	elseif love.keyboard.isDown('s') then
		player.y = player.y + (player.speed*dt)
	end
end

function collision(x1, x2, y1, y2, w1, w2, h1, h2)
	local xisect = false
	local yisect = false
	if (x1 <= x2 and (x1 + w1) >= x2) or (x1 <= (x2 + w2) and (x1 + w1) >= (x2 + w2)) then
		xisect = true
	end
	if (y1 <= y2 and (y1 + h1) >= y2) or (y1 <= (y2 + h2) and (y1 + h1) >= (y2 + h2)) then
		yisect = true	
	end

	return xisect and yisect
end

function initializeBlock(offset)
	local blk = {}
	blk.width = 20
	blk.height = 70
	blk.x = love.graphics.getWidth() + blk.width + (offset * (love.graphics.getWidth() / 3))
	blk.y = love.math.random(blk.height, love.graphics.getHeight())
	blk.speed = 300

	return blk
end

function drawMainMenu()
	love.graphics.draw(title.img, title.x, title.y, 0, 1, 1, 0, 0)
end

function drawPlaying()
	-- HUD
	love.graphics.setColor(0, 0, 0, 140)
	love.graphics.rectangle("fill", 10, 40, 100 * 2, 20);
	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.rectangle("fill", 10, 40, player.health * 2, 20);
	love.graphics.setColor(255, 255, 255, 255)


	-- Blue Player/AI boundary
	love.graphics.setColor(0, 0, 255, 140)
	love.graphics.rectangle("fill", player.x, player.y, player.width, player.height)
	love.graphics.setColor(255, 0, 0, 140)
	love.graphics.rectangle("fill", ai.x, ai.y, ai.width, ai.height)
	love.graphics.setColor(0, 0, 255, 140)
	love.graphics.rectangle("line", player.x, player.y, player.width, player.height)
	love.graphics.setColor(255, 0, 0, 140)
	love.graphics.rectangle("line", ai.x, ai.y, ai.width, ai.height)

	-- Reset draw color
	love.graphics.setColor(255, 255, 255, 255)

	-- Draw AI/Player	
	love.graphics.draw(ai.img, ai.x, ai.y, 0, 1, 1, 0, 0)
	if player.damaged ~= false then
		love.graphics.draw(player.img, player.x + (player.width / 2), player.y + (player.height / 2), player.orientation, 1, 1, ai.img:getWidth()/2, ai.img:getHeight()/2)
		player.orientation = player.orientation + 1
		if (gameTime - player.damaged) >= 1 then
			player.orientation = 0
			player.damaged = false
		end
	else
		love.graphics.draw(player.img, player.x, player.y, player.orientation, 1, 1, 0, 0)
	end

	-- Red rectangle
	love.graphics.setColor(255, 0, 0, 255)
	for i=1, BLOCK_COUNT, 1 do
		love.graphics.rectangle("fill", block[i].x, block[i].y, block[i].width, block[i].height)
	end

	-- Reset draw color
	love.graphics.setColor(255, 255, 255, 255)
end
